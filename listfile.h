#ifndef WRITELINE_H
#define WRITELINE_H

int formline(char* line, char* name, int watched, int have, char* group);
int parseline(char* line, char* name, int *watched, int *have, char* group);

#endif // WRITELINE_H

