#include <stdlib.h>
#include <wchar.h>

#include "writeintabs.h"

int calculatetabs(char* str, int tabwidth, int ntabs)
{
	wchar_t buf[1024];
	int width;
	int nlen;
	int diff;

	mbstowcs(buf, str, 1024);
	width = ntabs * tabwidth;
	nlen = wcswidth(buf, width - 1);
	diff = width - nlen;

	return ((diff - 1) / tabwidth) + 1;
}

