CC = gcc
CFLAGS = -c -Wall -ggdb -Werror -D_XOPEN_SOURCE

OUT = ep-organizer
SRCS = $(wildcard *.c)
OBJS = $(SRCS:.c=.o)

all: $(OUT)

$(OUT): $(OBJS)
	$(CC) -o $@ $^

%.o : %.c
	$(CC) -o $@ $(CFLAGS) $<

.PHONY: clean
clean:
	rm $(OUT) $(OBJS)
