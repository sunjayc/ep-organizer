#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

#include "writeintabs.h"
#include "listfile.h"

int main(int argc, char** argv)
{
	char buf[1024];
	char name[1024];
	char group[1024];
	int have, watched;
	FILE *fp;

	setlocale(LC_ALL, "");

	printf("formline test:\n");
	formline(buf, "Name of Series", 20, 22, "Subgroup");
	printf("%s", buf);
	formline(buf, "Name of Another Series", 3, 3, "joint-fs");
	printf("%s", buf);
	formline(buf, "SuperHugeLongNameOfASeriesThatIsSoSuperGreatYouDon'tEvenKnow", 52, 63, "haha");
	printf("%s", buf);
	printf("\n");

	fp = fopen("alist.txt", "r");
	if(fp == NULL)
		return 1;

	printf("parseline test:\n");
	while(fgets(buf, 1024, fp) != NULL)
	{
		if(buf[0] != '\n')
		{
			parseline(buf, name, &watched, &have, group);
			formline(buf, name, watched, have, group);
			printf("%s", buf);
		}
	}
	printf("\n");

	fclose(fp);

	return 0;
}

