#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "writeintabs.h"
#include "listfile.h"

int formline(char* line, char* name, int watched, int have, char* group)
{
	char format[1024];
	int i;
	int ntabs;

	sprintf(format, "%.31s", name); // The name should be at most 31 characters so that there is at least one tab
	ntabs = calculatetabs(format, 8, 4);
	for(i = 0; i < ntabs; i++)
		strcat(format, "\t");
	strcat(format, "ep%02d/%02d");
	for(i = 0; i < (have - watched); i++)
		strcat(format, "<");
	strcat(format, "\t\t\t[%s]\n");

	// format is something like: "<name>		ep%02d/%02d<<			[%s]"
	return sprintf(line, format, watched, have, group);
}

int parseline(char* line, char* name, int *watched, int *have, char* group)
{
	int i;
	char buf[32];

	for(i = 0; *line != '\t'; i++, line++) // <name>
		name[i] = *line;
	name[i] = '\0';
	while(*line == '\t') // padding to 32
		line++;
	line += 2; // 'ep'
	for(i = 0; *line != '/'; i++, line++) // <watched>
		buf[i] = *line;
	buf[i] = '\0';
	*watched = atoi(buf);
	line++; // '/'
	for(i = 0; *line != '\t'; i++, line++) // <have>, plus any '<' or '*'
		buf[i] = *line;
	buf[i] = '\0';
	*have = atoi(buf);
	for(i = 0; i < 3; line++) // capture a total of three '\t'
	{
		if(*line == '\t')
			i++;
	}
	line++; // '['
	for(i = 0; *line != ']'; i++, line++) // <group>
		group[i] = *line;
	group[i] = '\0';

	return 0;
}

